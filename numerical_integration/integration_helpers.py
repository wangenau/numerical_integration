#!/usr/bin/env python3
'''
Implementation of different numerical integration methods with
functions to display them.
'''

from inspect import getsourcelines, signature
from numpy.random import uniform
from scipy.integrate import quad
from scipy.special import factorial
from sympy import integrate, lambdify
import matplotlib.pyplot as plt
import numpy as np


def int_sympy(f, a, b, x):
    '''Analytical integration of f from a to b via sympy.integrate.
    Args:
        f : sympy function
            Function to integrate. Should only depend on x.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        x : sympy symbol
            Symbol that f depends on.
    Returns:
        Integration value as a float.
    '''
    F = integrate(f, x)
    F = lambdify(x, F, modules='numpy')
    return F(b) - F(a)


def int_midpoint(f, a, b, n):
    '''Numerical integration of f from a to b via chained midpoint rule.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of rectangles. Has to larger than zero.
    Returns:
        Integration value as a float.
    '''
    width = (b - a) / n
    mi = np.arange(a + width / 2, b, width)
    return width * np.sum(f(mi))


def int_trapez(f, a, b, n):
    '''Numerical integration of f from a to b via chained trapezoidal rule.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of trapezoids. Has to larger than zero.
    Returns:
        Integration value as a float.
    '''
    width = (b - a) / n
    xi = np.arange(a + width, b, width)
    return 0.5 * width * (f(a) + np.sum(2 * f(xi)) + f(b))


def int_gauss(f, a, b, n):
    '''Numerical integration of f from a to b via Gauss-Legendre quadrature.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of integration points. Has to larger than zero.
    Returns:
        Integration value as a float.
    '''
    prefactors = [0] * (n + 1)
    for k in range(n // 2 + 1):
        prefactors[2 * k] = (-1)**k * factorial(2 * (n - k)) / \
                            (factorial(n - k) * factorial(n - 2 * k) * factorial(k) * 2**n)
    pol = np.poly1d(prefactors)
    dpol = pol.deriv()
    roots = pol.r
    weights = 2 / ((1 - roots**2) * dpol(roots)**2)
    return 0.5 * (b - a) * np.sum(weights * f(0.5 * (b - a) * roots + 0.5 * (a + b)))


def int_mc(f, a, b, n):
    '''Numerical integration of f from a to b via Monte Carlo integration.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of sample points. Has to larger than zero.
    Returns:
        Integration value as a float.
    '''
    xi = uniform(a, b, n)
    return np.sum(f(xi)) * (b - a) / n


def int_quad(f, a, b, **kwargs):
    '''Numerical integration of f from a to b via scipy.integrate.quad.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
    Returns:
        Integration value as a float.
    '''
    F, _ = quad(f, a, b)
    return F


def plot_int(f, a, b, **kwargs):
    '''Plot f and fill out the integral from a to b.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
    '''
    if a >= b:
        print('WARNING: b should be larger than a to display function and integral correct.')
    # Add 25% of the integration interval left and right to the view
    extra = (b - a) * 0.25
    x_func = np.linspace(a - extra, b + extra, 1000)
    y_func = f(x_func)
    x_int = np.linspace(a, b, 1000)
    y_int = f(x_int)

    plt.figure(figsize=(12, 6))
    plt.fill_between(x_int, 0, y_int, alpha=0.5, label='Integral')
    plt.plot(x_func, y_func, color='k', label='Function')
    plt.xlim(a - extra, b + extra)
    plt.grid(ls='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x', fontsize=16)
    plt.ylabel('f(x)', fontsize=16)
    plt.legend(fontsize=14)
    plt.show()
    plt.close()
    return


def plot_midpoint(f, a, b, n):
    '''Plot f and fill out the rectangles from the chained midpoint rule.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of rectangles. Has to larger than zero.
    '''
    plt.figure(figsize=(12, 6))
    if a >= b:
        print('WARNING: b should be larger than a to display function and rectangles correct.')
    # Plot rectangles
    width = (b - a) / n
    x = np.arange(a, b + width, width)
    for i in range(n):
        x_int = [x[i], x[i + 1]]
        y_int = [f(x[i] + width / 2)] * len(x_int)
        plt.fill_between(x_int, 0, y_int, alpha=0.5, label='Rectangles' if i == 0 else '',
                         color='tab:blue')
    # Add 25% of the integration interval left and right to the view
    extra = (b - a) * 0.25
    x_func = np.linspace(a - extra, b + extra, 1000)
    y_func = f(x_func)

    plt.plot(x_func, y_func, color='k', label='Function')
    plt.xlim(a - extra, b + extra)
    plt.grid(ls='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x', fontsize=16)
    plt.ylabel('f(x)', fontsize=16)
    plt.legend(fontsize=14)
    plt.show()
    plt.close()
    return


def plot_trapez(f, a, b, n):
    '''Plot f and fill out the trapezoids from the chained trapezoidal rule.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of trapezoids. Has to larger than zero.
    '''
    plt.figure(figsize=(12, 6))
    if a >= b:
        print('WARNING: b should be larger than a to display function and trapezoids correct.')
    # Account for potential division by zero
    else:
        # Plot trapezoidals
        width = (b - a) / n
        x = np.arange(a, b + width, width)
        for i in range(n):
            m = (f(x[i + 1]) - f(x[i])) / (x[i + 1] - x[i])
            n = (x[i + 1] * f(x[i]) - x[i] * f(x[i + 1])) / (x[i + 1] - x[i])
            x_int = np.linspace(x[i], x[i + 1], 1000)
            y_int = m * x_int + n
            plt.fill_between(x_int, 0, y_int, alpha=0.5, label='Trapezoids' if i == 0 else '',
                             color='tab:blue')
    # Add 25% of the integration interval left and right to the view
    extra = (b - a) * 0.25
    x_func = np.linspace(a - extra, b + extra, 1000)
    y_func = f(x_func)

    plt.plot(x_func, y_func, color='k', label='Function')
    plt.xlim(a - extra, b + extra)
    plt.grid(ls='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x', fontsize=16)
    plt.ylabel('f(x)', fontsize=16)
    plt.legend(fontsize=14)
    plt.show()
    plt.close()
    return


def plot_mc(f, a, b, n):
    '''Plot f and show randomly sampled vlines for Monte Carlo integration.
    Args:
        f : function
            Function to integrate.
        a : float
            Lower integration bound.
        b : float
            Upper integration bound.
        n : int
            Number of sample points. Has to larger than zero.
    '''
    if a >= b:
        print('WARNING: b should be larger than a to display function and trapezoid correct.')
    # Create a random sample (will not be the same to the one used int_mc)
    xi = uniform(a, b, n)
    # Add 25% of the integration interval left and right to the view
    extra = (b - a) * 0.25
    x_func = np.linspace(a - extra, b + extra, 1000)
    y_func = f(x_func)

    plt.figure(figsize=(12, 6))
    plt.vlines(xi, [0] * len(xi), f(xi), alpha=0.5, label='Sample', lw=1)
    plt.plot(x_func, y_func, color='k', label='Function')
    plt.xlim(a - extra, b + extra)
    plt.grid(ls='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel('x', fontsize=16)
    plt.ylabel('f(x)', fontsize=16)
    plt.legend(fontsize=14)
    plt.show()
    plt.close()
    return


def print_function(func):
    '''Get the body of a python function as a markdown formattable string.
    Adapted from https://stackoverflow.com/a/57035304.
    Args:
        func : python function
            Function to print.
    Returns:
        Markdown formattable function source code without docstring.
    '''
    name = func.__name__
    sig = str(signature(func))
    try:
        lines_to_skip = len(func.__doc__.split('\n'))
    except AttributeError:
        lines_to_skip = 0
    lines = getsourcelines(func)[0]
    return '```python\ndef ' + name + sig + ':\n' + ''.join(lines[lines_to_skip + 1:]) + '```'
