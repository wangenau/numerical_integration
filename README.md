# Numerical Integration
Learn something about numerical integration schemes.

This repository will give examples of how different integration methods can be used or implemented in Python.

The used methods are
- [sympy.integrate](https://docs.sympy.org/latest/modules/integrals/integrals.html)
- [Midpoint rule](https://en.wikipedia.org/wiki/Riemann_sum#Midpoint_rule)
- [Trapezoidal rule](https://en.wikipedia.org/wiki/Trapezoidal_rule)
- [Gauss-Legendre quadrature](https://en.wikipedia.org/wiki/Gauss%E2%80%93Legendre_quadrature)
- [Monte Carlo integration](https://en.wikipedia.org/wiki/Monte_Carlo_integration)
- [scipy.integrate.quad](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.quad.html)

## [Webpage](https://wangenau.gitlab.io/numerical_integration)
Due to some displaying issues with notebooks in GitLab, an HTML webpage has been created.

## Requirements
Jupyter with a Python kernel of version 3.6 or higher is required. For package dependencies see the [requirements.txt](requirements.txt) file for details. To install them, use:
```
pip3 install -r requirements.txt
```

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
